function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
    <div class="shadow p-3 mb-5 bg-body-tertiary-rounded">
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h5 class="card-subtitle mb-2 text-muted">${location}</h5>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">${starts} - ${ends} </div>
      </div>
      
      </div>
    `;
  }
  function formatDate(date) {
    const x = new Date(date);
    console.log(x)
    const format = `${x.getMonth() + 1}/${x.getDate()}/${x.getFullYear()}`;
    return format;
  }

  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        
      } else {
        const data = await response.json();
        const conferences = data.conferences;
  
        const columns = document.querySelectorAll('.col');
        //need to set the column index to 0 
        let columnIndex = 0;
  
        for (let i = 0; i < conferences.length; i++) {
          const conference = conferences[i];
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
  
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            console.log(details)
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const starts = formatDate(details.conference.starts)
            const ends = formatDate(details.conference.ends)
            const location = details.conference.location.name
            const html = createCard(title, description, pictureUrl, starts, ends, location);
  
            columns[columnIndex].innerHTML += html;
            console.log(columns.length)
  
            columnIndex = (columnIndex + 1) % columns.length;
          }

        }
        
      }
    } catch (e) {
     
      console.error(e);
    }
  
  });

  

